(function(){
	angular.module('myApp',['ui.router'])
		.config(function($stateProvider, $urlRouterProvider) {
  			$urlRouterProvider.otherwise("home");
  			$stateProvider
		    .state('home', {
		      url: "/home",
		      templateUrl: "public/features/home/home.html",
		      controller: "homeCtrl as vm"
		    });
		  });
})();
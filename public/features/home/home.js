(function(){
    'use strict';
	angular.module('myApp')
		.controller('homeCtrl',homeCtrl);

		homeCtrl.$inject = ['homeService','$scope','$http'];

		function homeCtrl(homeService,$scope,$http)
		{
				var vm=this;
				vm.data=[];
				vm.impData=impData;
				vm.dis=dis;

				$scope.data={
					setData:{},
					show:'login',
					token:{}
				};

				impData();
				function impData()
				{
					$http({
					  method: 'GET',
					  url:'index.php/user/token'
					}).success(function(resp){
						$scope.data.token['CSRF']=resp;
						console.log($scope.data.token);
					});

				}
				function dis()
				{
						$http({
					  	method: 'POST',
					  	url:'index.php/user/save',
					  	headers:$scope.data.token,
					  	data:$scope.data.setData
						}).success(function(resp){
							console.log(resp);
						});						
						
				}

		}

})();
		
(function(){
	    'use strict';
	angular.module('myApp')
	.factory('homeService', homeService);

	function homeService()
	{
		var data={
			getData:getData
		};
		return data;
		function getData()
		{
		   return [
                {first: 'John', last: 'Papa', lunchMoney: 58.1234},
                {first: 'John', last: 'Smith', lunchMoney: 8.34},
                {first: 'Troy', last: 'Hamilton', lunchMoney: 18.747},
                {first: 'Anthony', last: 'Tornatore', lunchMoney: 38.4},
                {first: 'Pam', last: 'Dale', lunchMoney: 4.003}
            ];
		}

	}
})();
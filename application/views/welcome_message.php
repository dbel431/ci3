<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	
	<script src="assets/js/angular.min.js"></script>
	<script src="assets/js/angular-ui-router.min.js"></script>
	<script src="public/moduleApp.js"></script>
	<script src="public/features/mainCtrl.js"></script>
	<!--  -->
	<script src="public/features/home/home.js"></script>

	<script src="public/features/home/homeService.js"></script>
	
	<!--  -->
</head>
<body data-ng-app="myApp">
<div data-ng-controller="mainCtrl">
	<ui-view> </ui-view>
</div>
</html>
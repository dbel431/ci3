<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();  
		$this->load->model('User_model');
	//	$this->load->library('UserTran');
	}
	public function getAll()
	{
		$data=$this->User_model->getAll();
	//	$resp=$this->UserTran->zxc($data);
		echo json_encode($data);
	}
	public function save()
	{
		$data=array();
		
        		//$data=$this->input->post();
		$data = $this->security->xss_clean($data);
		
		echo json_encode($data);
		/*$data = $this->security->xss_clean($data);
		echo json_encode($data);*/
	}
	public function token()
	{
			echo $this->security->get_csrf_hash();	
	}
}
